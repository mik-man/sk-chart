<?
  header("Content-Type: text/html; charset=utf-8");
  include "descriptionTable.php";
  include "dataTable.php";
  include "chart.php";
  $chainProps = [
    "guarantee" => 1200, // thousands tons
    "limit" => 3, // percents
  ];
  // between 0 and 1 ( 1 - trend line always ends at guarantee point, 0 - don't include guarantee in the calculations at all )
  $tendingToGuaranteeCoef = 0.75;
  $enableTrendLineShift = true;
  $descriptionTable = array(
    "Степная", "141", "26х92", "плоская", "", "100.000", "510", $chainProps['limit'], "2020.01.01", "0.000");
  $dataTable = array(
    array("2020.01.01", 510, 0.00, 0, "ФИО"),
    array("2020.01.03", 513.92, 0.77, 155, "ФИО"),
    array("2020.01.05", 514.28, 0.83, 245, "ФИО"),
    array("2020.01.06", 515.6, 1.09, 444.75, "ФИО"),
    // - - - - -
//     array("2020.01.01", 510, 0.00, 0, "ФИО"),
//     array("2020.01.03", 510.14, 0.12, 160, "ФИО"),
//     array("2020.01.05", 513.52, 0.33, 305, "ФИО"),
//     array("2020.01.05", 516.84, 0.33, 450, "ФИО"), // approx calculated
  );
?>
<html>

<head>
  <title>Chart page</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
  <div class="row">
    <div>
      <? renderDescriptionTable($descriptionTable); ?>
    </div>
    <div>
      <? renderDataTable($dataTable); ?>
    </div>
  </div>
  <div class="row custom-wrap">
    <div id="chart-section">
      <h3>Прогноз износа цепи</h3>
      <div id="chart-container">
        <? renderChart($descriptionTable, $dataTable, $chainProps, $tendingToGuaranteeCoef, $enableTrendLineShift); ?>
      </div>
    </div>
    <? include "chartExplanation.php" ?>
  </div>
</body>

</html>
