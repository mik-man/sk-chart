var store = {
  tendingToGuaranteeCoef: 0.5,
	limitPercent: 3,
  guarantee: 750,
	pointsCount: 3,
	data: [{ x: 95, y: 736 }, { x: 300, y: 738 }, { x: 550, y: 743 }],
  trendData: [{ x: 95, y: 736 }, { x: 300, y: 738 }, { x: 550, y: 743 }],
	dates: ['2020.08.01', '2020.08.02', '2020.08.03'],
	angles: [45],
	cross: { x: 0, y: 0 },
	crossAngle: 45,
	limit: [],
	vline: [],
	pointLines: [] // 2 dimentions array
};

// example
function callRenderChart() {
	bootstrapChart(
		'chart',
		[{ x: 95, y: 736 }, { x: 300, y: 738 }, { x: 550, y: 743 }],
		['2020.07.01', '2020.07.02', '2020.07.03'],
		5,
		1.3,
	);
}

function Trend(data, chainProps, tendingToGuaranteeCoef, enableTrendLineShift) {
  const { limit, guarantee} = chainProps;

  const limitY = data[0].y * (100 + limit) / 100;

  const inputData = [
    ...data.map(point => [point.x, point.y]),
  ];
  const trendData = regression.exponential(
    inputData,
    {
      order: 2,
      precision: 10,
    },
  );
  const [a, b] = trendData.equation;
  const shift = enableTrendLineShift ? data[0].y - trendData.predict(0)[1] : 0;
  function calcB(a, B, g, shift) {
    const x = Math.log((limitY - shift)/a)/B;
    if ((g - x) >= 0) { return B; }
    const xt = x + ((g - x) * tendingToGuaranteeCoef);
    return Math.log((limitY - shift)/a)/xt;
  }

  const bq = calcB(a, b, guarantee, shift);
  this.calcX = function(y) {
    return Math.log((y - shift)/a)/bq;
  }
  this.calcY = function(x) {
    return shift + (a*Math.exp(x * bq));
  }
  this.toString = function() {
    return trendData.string;
  }
  this.trendPoints = function(Ybegin, Yend, step) {
    const points = [];
    const y0 = this.calcY(0);
    points.push({ y: y0, x: 0 });

    for (let y = y0 + step; y <= Yend; y += step) {
      points.push({ y, x: this.calcX(y) });
    }
    if (points[points.length - 1].y < Yend) {
      points.push({ y: Yend, x: this.calcX(Yend) });
    }
    return points;
  }
}

function bootstrapChart(rootEl, points, dates, chainProps, tendingToGuaranteeCoef, enableTrendLineShift) {
	store.data = points;
	store.dates = dates;
	const trend = new Trend(points, chainProps, tendingToGuaranteeCoef, enableTrendLineShift);
  store.trendData = trend.trendPoints(points[0].y, 527, 4);
	store.limitPercent = chainProps.limit;
	store.guarantee = chainProps.guarantee;
	store.tendingToGuaranteeCoef = tendingToGuaranteeCoef;
	store.pointsCount = points.length;
	const errorsList = checkData();
	if (errorsList.length) {
	  printInputDataError(rootEl, errorsList);
    return;
  }
	calcData(trend);
	window.addEventListener('load', () => {
    setTimeout(() => drawChart(`#${rootEl}`), 0); // workaround for android chrome - this browser calls the load event before content rendering complete
  } );
	window.addEventListener('resize', () => { drawChart(`#${rootEl}`); } );
}

function printInputDataError(parentElId, errorsList) {
  const chartEl = document.querySelector(`#${parentElId}`);
  if (!chartEl) { throw new Error('Invalid root element selector'); }
  const parentEl = chartEl.parentNode;
  const title = document.createElement('p');
  title.textContent = 'Ошибка входных данных, обратитесь к администратору';
  const error = document.createElement('p');
  error.textContent = JSON.stringify(errorsList);
  parentEl.insertBefore(title, chartEl);
  parentEl.insertBefore(error, chartEl);
}

function drawChart(svgId) {
	let { data, trendData, limit, vline } = store;

	const svg = d3.select(svgId);
	svg.selectAll("*").remove();
	const svgWidth = svg.node().width.animVal.value;
	const svgHeight = svg.node().height.animVal.value;
	const margin = { top: 20, right: 20, bottom: 30, left: 50 },
		width = +svgWidth - margin.left - margin.right,
		height = +svgHeight - margin.top - margin.bottom,
		g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	let xScale = d3.scaleLinear().rangeRound([0, width - 50]);
	let yScale = d3.scaleLinear().rangeRound([height, 0]);
	xScale.domain([limit[0].x, limit[1].x]);
	yScale.domain([vline[0].y, vline[1].y]);

	g.append("g")
		.attr("transform", "translate(0," + height + ")")
		.call(d3.axisBottom(xScale));

	g.append("g").call(d3.axisLeft(yScale));

	let line = d3.line()
		.x(function (d) { return xScale(d.x) })
		.y(function (d) { return yScale(d.y) });

	let curveLine = d3.line()
		.x(function (d) { return xScale(d.x) })
		.y(function (d) { return yScale(d.y) })
		.curve(d3.curveMonotoneX);

	// limit line sign
	g.append("text").attr("x", 160).attr("y", 16)
		.text(`limit = ${store.limit[0].y} (${store.limitPercent}%)`)
		.attr("font-size", ".8em");

	// limit
	g.append("path")
		.datum(limit)
		.attr("fill", "none")
		.attr("stroke", "black")
		.attr("stroke-width", .6)
		.attr("d", line);

	// vertical line
	g.append("path")
		.datum(vline)
		.attr("fill", "none")
		.attr("stroke", "black")
		.attr("d", line);

	// chart line
	g.append("path")
		.datum(trendData)
		.attr("fill", "none")
		.attr("stroke", "black")
		.style("stroke-dasharray", ("3, 3"))
		.attr("d", curveLine);

	// without tail point
	const points = data.slice(0, store.pointsCount + 1);

	// points
	g.selectAll()
		.data(points)
		.enter()
		.append("circle")
		.attr("stroke", "none")
		.attr("cx", function (d) { return xScale(d.x) })
		.attr("cy", function (d) { return yScale(d.y) })
		.attr("r", 3);

	// points sings
	g.selectAll()
		.data(points)
		.enter()
		.append("text")
		.attr("x", function (d, i) { return xScale(d.x) + shiftX(i) })
		.attr("y", function (d, i) { return yScale(d.y) + shiftY(d, i) })
		.text(getSign) // points hints
		.attr("font-size", ".8em");

	// points coordinates lines
	for (let pli = 0; pli < store.pointsCount; pli++) {
		drawPointLine(g, pli, line);
		drawPointLineSigns(g, pli, xScale, yScale);
	}
}

function drawPointLine(g, pli, line) {
	const { pointLines } = store;
	g.append("path")
		.datum(pointLines[pli])
		.attr("fill", "none")
		.attr("stroke", "blue")
		.attr("d", line)
		.attr("stroke-width", .4);
}

function drawPointLineSigns(g, pli, xScale, yScale) {
	const { pointLines } = store;
	g.selectAll()
		.data(pointLines[pli])
		.enter()
		.append("text")
		.attr("fill", "blue")
		.attr("font-size", ".8em")
		.attr("x", function (d, i) { return xScale(d.x) + 5 })
		.attr("y", function (d, i) { return yScale(d.y) - 5 })
		.text(getPointLineSign); // points hints

}

function getPointLineSign(point, i) {
	switch (i) {
		case 0: return `${point.y}`;
		case 1: return '';
		case 2: return `${point.x}`;
	}
}

function getSign(point, i) {
	const { dates } = store;
	switch (i) {
		case store.pointsCount:
			return (`X = ${Math.round(point.x * 100) / 100}`);
		default:
			return (`Z${i + 1} ${dates[i]}`);
	}
}

function shiftX(i) {
	return 7;
}

function shiftY(p, i) {
	if (i === store.pointsCount) return 18;
	return 6;
}

function checkData() {
	const { data, pointsCount } = store;
	const checkErrors = [];
	let limitPercent = store.limitPercent;
	if (limitPercent < 3 || limitPercent > 100) {
		checkErrors.push(`Limit should be from 3% to 100%`);
		limitPercent = 3;
		store.limitPercent = limitPercent;
	}
	const limit = data[0].y * (100 + limitPercent) / 100;
	for (let i = 1; i < pointsCount; i++) {
		if (data[i].x <= data[i - 1].x) {
      checkErrors.push(`z${i + 1}.x should be more than z${i}.x!`);
			data[i].x = data[i - 1].x + 1;
		}
		if (data[i].y <= data[i - 1].y) {
      checkErrors.push(`z${i + 1}.y should be more than z${i}.y!`);
			data[i].y = data[i - 1].y + 1;
		}
		if (data[i].y >= limit) {
      checkErrors.push(`z${i + 1}.y should be less than z1.y + ${limitPercent}% (${limit})`);
			data[i].y = Math.round(limit * 0.99 * 10) / 10;
		}
	}
	return checkErrors;
}
